var answers = ["Бесспорно","Безусловно","Никаких сомнений","Определенно да","Можешь быть уверен  этом", "Мне кажется да","Вероятнее всего","Хорошие шансы на это","Да","Скорее да, чем нет", "Пока не ясно, попробуй<br/>снова","Спроси позже","Сейчас нельзя предсказать","Спроси опять","Это невозможно предсказать","Даже не думай<br/>об этом","Нет","Перспективы не очень<br/>хорошие","Весьма сомнительно","Скорее нет,чем да"]


var answer = document.getElementsByTagName("figcaption")[0];

window.addEventListener('devicemotion', getDeviceMotion, false);
var threshold = 10; //default velocity threshold for shake to register
var timeout = 1000; //default interval between events
var lastTime = new Date(); //use date to prevent multiple shakes firing

//accelerometer values
var lastX = null;
var lastY = null;
var lastZ = null;

function getDeviceMotion(event) {
  var current = event.accelerationIncludingGravity;
  var currentTime, timeDifference, index;
  var deltaX = 0;
  var deltaY = 0;
  var deltaZ = 0;

  if ((lastX === null) && (lastY === null) && (lastZ === null)) {
      lastX = current.x;
      lastY = current.y;
      lastZ = current.z;
      return;
  }

  deltaX = Math.abs(lastX - current.x);
  deltaY = Math.abs(lastY - current.y);
  deltaZ = Math.abs(lastZ - current.z);

  if (((deltaX > threshold) && (deltaY > threshold)) || ((deltaX > threshold) && (deltaZ > threshold)) || ((deltaY > threshold) && (deltaZ > threshold))) {
      //calculate time in milliseconds since last shake registered
      currentTime = new Date();
      timeDifference = currentTime.getTime() - lastTime.getTime();

      if (timeDifference > timeout) {
          index = Math.floor(Math.random() * answers.length);
          answer.innerHTML = answers[index];
          lastTime = new Date();
      }
  }

  lastX = current.x;
  lastY = current.y;
  lastZ = current.z;
};
